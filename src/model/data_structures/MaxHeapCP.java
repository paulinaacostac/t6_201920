package model.data_structures;

public class MaxHeapCP <T extends Comparable<T>>
{
	private T[] elementos;
	private int tamanoMax;
	private int tamano;
	
	public MaxHeapCP()
	{
		int max=100;
		elementos = (T[]) new Comparable[max+1];
		tamanoMax=max;
		tamano=0;
	}
	
	public MaxHeapCP(int max)
	{
		elementos = (T[]) new Comparable[max+1];
		tamanoMax=max;
		tamano=0;
	}
	public void insertar(T elemento)
	{
		elementos[++tamano] = elemento;
		swim(tamano);
		if(tamanoMax == tamano) resize(2*tamanoMax);
	}
	public T sacarMax()
	{
		T max = elementos[1]; 
		exch(1, tamano--); 
		elementos[tamano+1] = null; 
		sink(1);
		return max;
	}
	public T darMax()
	{
		return elementos[1];
	}
	
	public boolean esVacia()
	{
		return tamano==0;
	}
	public int size()
	{
		return tamano;
	}
	private boolean less(int i, int j)
	{
		return elementos[i].compareTo(elementos[j]) < 0; 
	}
	private void exch(int i, int j)
	{
		T t = elementos[i];
		elementos[i] = elementos[j];
		elementos[j] = t; 
	}
	private void swim(int k)
	{
		while (k > 1 && less(k/2, k))
		{
			exch(k/2, k);
			k = k/2;
		}
	}
	private void sink(int k)
	{
		while (2*k <= tamano)
		{
			int j = 2*k;
			if (j < tamano && less(j, j+1)) j++;
			if (!less(k, j)) break;
			exch(k, j);
			k = j;
		}
	}
	private void resize(int max)
	{
		
		T[] newArray = (T[]) new Comparable[max+1];
		for (int i = 1; i < elementos.length; i++) 
		{
			newArray[i] = elementos[i];
		}
		tamanoMax=max;
		elementos = newArray;
	}


}
