package model.data_structures;

import model.logic.Viaje;

public class Esquina implements Comparable<Esquina>
{
	private int idNodo;
	private double longitud;
	private double latitud;
	
	public Esquina(int pIdNodo,double pLongitud,double pLatitud)
	{
		idNodo=pIdNodo;
		longitud=pLongitud;
		latitud=pLatitud;
	}
	public int darpIdNodo()
	{
		return idNodo;
	}
	public double darLongitud()
	{
		return longitud;
	}
	public double darLatitud()
	{
		return latitud;
	}
	@Override
	public int compareTo(Esquina arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

}
