package model.logic;

import model.data_structures.ArregloDinamico;

public class Zona implements Comparable<Zona>
{
	private Integer sourceId;
	private String nombre;
	private double shape_leng;
	private double shape_area;
	private ArregloDinamico<Coordenada> coordenadas;
	
	public Zona(Integer pSourceid,String pNombre, double shape_leng2, double shape_area2,ArregloDinamico<Coordenada> pCoord)
	{
		sourceId=pSourceid;
		nombre=pNombre;
		shape_leng=shape_leng2;
		shape_area=shape_area2;
		coordenadas=pCoord;
	}
	public Integer darSourceId()
	{
		return sourceId;
	}
	public String darNombre()
	{
		return nombre;
	}
	public double darPerimetro()
	{
		return shape_leng;
	}
	public double darArea()
	{
		return shape_area;
	}
	public ArregloDinamico<Coordenada> darCoordenadas()
	{
		return coordenadas;
	}
	public int darNumeroCoordenadas()
	{
		return darCoordenadas().darTamano();
	}

	@Override
	public int compareTo(Zona o) {
		// TODO Auto-generated method stub
		return 0;
	}

}
