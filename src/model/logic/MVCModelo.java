package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.Iterator;

import com.opencsv.CSVReader;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import model.data_structures.ArregloDinamico;
import model.data_structures.Esquina;
import model.data_structures.RedBlackBST;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo
{
	/**
	 * Atributos del modelo del mundo
	 */
	private int totalViajesHora;
	private int totalViajesDia;
	private int totalViajesMes;
	private int numeroViajes;
	private int numeroZonas;
	private int numeroNodos;	
	private Zona minimo;
	private Zona maximo;
	private ArregloDinamico<Zona> zonas;
	private RedBlackBST<Integer,Zona> zonasRB;

	public MVCModelo()
	{

		zonas = new ArregloDinamico<Zona>(500);
		zonasRB=new RedBlackBST<Integer,Zona>();
		totalViajesDia=0;
		totalViajesHora=0;
		totalViajesMes=0;
		numeroNodos=0;
		numeroViajes=0;
		numeroZonas=0;
		maximo=null;
		minimo=null;

	}
	public void cargarZona()
	{
		Gson gson = new Gson();
		int min=Integer.MAX_VALUE;
		int max=0;
		try (Reader reader = new FileReader("./data/bogota_cadastral.json")) 
		{
			ArregloJSON az=gson.fromJson(reader, ArregloJSON.class);
//			System.out.println(az.darFeatures()[0].getProperties().toString());
			numeroZonas=az.darFeatures().length;
			for (int i=0;i<az.darFeatures().length;i++)
			{
				Integer sourceId=Integer.parseInt(az.darFeatures()[i].getProperties().getMOVEMENTID());
				if (sourceId>max)
				{
					max=sourceId;
				}
				if (sourceId<min)
				{
					min=sourceId;
				}
				String nombre=az.darFeatures()[i].getProperties().getScanombre();
				double shape_leng=az.darFeatures()[i].getProperties().getShapeLeng();
				double shape_area=az.darFeatures()[i].getProperties().getShapeArea();
				double longitud=0.0;
				double latitud=0.0;
				double[][][][] coordenadas=az.darFeatures()[i].getGeometry().getCoordinates();
				int numCoordenadas=coordenadas[0][0].length;
				ArregloDinamico<Coordenada> coordenadasD=new ArregloDinamico<>(700);
				for (int j=0;j<numCoordenadas;j++)
				{
						longitud=coordenadas[0][0][j][0];
						latitud=coordenadas[0][0][j][1];
						Coordenada coord=new Coordenada(longitud,latitud);
						coordenadasD.agregar(coord);
//						System.out.println(j);
//						System.out.println("longitud: "+longitud+" latitud: "+latitud);
				}
				Zona zona=new Zona(sourceId, nombre, shape_leng, shape_area, coordenadasD);
				zonasRB.put(sourceId,zona);
			}
			minimo=zonasRB.get(min);
			maximo=zonasRB.get(max);
//			System.out.println(zonas.darElemento(1).darCoordenadas().darElemento(0).darLatitud()+" Latitud");
//			System.out.println(zonas.darElemento(1).darCoordenadas().darElemento(0).darLongitud()+" Longitud");

		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
	}
	public Zona darMaximo()
	{
		return maximo;
	}
	public int darNumeroZonas()
	{
		return numeroZonas;
	}
	public Zona darMinimo()
	{
		return minimo;
	}
	public Zona consultarZonaPorId(Integer pId)
	{
		Zona r=zonasRB.get(pId);
		if (r==null)
		{
			System.out.println("El id no existe");
		}
		return r;
	}
	public ArregloDinamico<Zona> ConsultaZonasenRango(Integer IdMin,Integer IdMax)
	{
		Zona z=null;
		ArregloDinamico<Zona> zonass=new ArregloDinamico<>(1000);
		Iterator<Integer> iter=zonasRB.keysInRange(IdMin, IdMax);
		while (iter.hasNext())
		{
			Integer s=iter.next();
			z=zonasRB.get(s);
			if (z==null)
			{
				System.out.println("La zona con llave"+s+" no existe");
			}
			zonass.agregar(z);
		}
		return zonass;
	}
	
	public int nodosRedBlack()
	{
		return zonasRB.size();
	}
	
	public int alturaRedBlack()
	{
		return zonasRB.height();
	}
	
	public double alturaPromedioHojas()
	{
		Iterator<Integer> keys=zonasRB.getLeafs();
		int num =0;
		int suma=0;
		while (keys.hasNext()) 
		{
			Integer key = (Integer) keys.next();
			suma+=zonasRB.getHeight(key);
			num++;
		}
		return (double)suma/(double)num;
	}
}
