package model.logic;

public class Coordenada implements Comparable<Coordenada>
{
	private double longitud;
	private double latitud;
	public Coordenada(double pLongitud,double pLatitud)
	{
		longitud=pLongitud;
		latitud=pLatitud;
	}
	public double darLongitud()
	{
		return longitud;
	}
	public double darLatitud()
	{
		return latitud;
	}
	@Override
	public int compareTo(Coordenada o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
