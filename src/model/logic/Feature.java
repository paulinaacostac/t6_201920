package model.logic;

public class Feature 
{
	private String type;
	private Geometry geometry;
	private Properties properties;
	
	public Geometry getGeometry()
	{
		return geometry;
	}
	public Properties getProperties()
	{
		return properties;
	}

}
