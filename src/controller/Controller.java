package controller;


import java.util.Scanner;

import model.data_structures.ArregloDinamico;
import model.logic.MVCModelo;
import model.logic.Viaje;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;

	/* Instancia de la Vista*/
	private MVCView view;

	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo=new MVCModelo();
	}

	public void run() 
	{
		int tamanoInicial = 4001;
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		ArregloDinamico<Viaje> viajesHora=null;
		Integer dato1 = 0;
		Integer dato2 = 0;
		String respuesta = "";
		String respuesta1 = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				System.out.println("--------- \nCargando... ");
				modelo.cargarZona();
				System.out.println("Archivo CSV cargado");
				view.printCargarZonas(modelo.darNumeroZonas(), modelo.darMinimo(),modelo.darMaximo());
				break;
			case 2:
				System.out.println("--------- \nDar id");
				dato1=lector.nextInt();
				System.out.println("Zona buscada");
				view.printConsultarPorId(modelo.consultarZonaPorId(dato1));
				break;
			case 3:
				System.out.println("--------- \nDar id inferior");
				dato1=lector.nextInt();
				System.out.println("--------- \nDar id superior");
				dato2=lector.nextInt();
				view.printConsultarRangoPorId(modelo.ConsultaZonasenRango(dato1, dato2));
				break;
			case 4:
				view.printDatosAnalisis(modelo.nodosRedBlack(), modelo.alturaRedBlack(), modelo.alturaPromedioHojas());
				break;
			case 5:
				System.out.println("Salida");
				break;
			default: 
				System.out.println("--------- \n Opcion Invalida !! \n---------");
				break;
			}
		}

	}	
}
