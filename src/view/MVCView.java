package view;

import model.data_structures.ArregloDinamico;
import model.logic.MVCModelo;
import model.logic.Viaje;
import model.logic.Zona;

public class MVCView 
{
	/**
	 * Metodo constructor
	 */
	public MVCView()
	{
	}

	public void printMenu()
	{
		System.out.println("1. Cargar archivo CSV");
		System.out.println("2. Buscar Zona por Id");
		System.out.println("3. Buscar Zonas por rango de Id");
		System.out.println("4. Datos para an�lisis");
		System.out.println("5. Exit");
		System.out.println("Dar el numero de opcion a resolver, luego oprimir tecla Return: (e.g., 1):");
	}

	public void printCargarZonas(int pViajes, Zona pPrimerViaje, Zona pUltimoViaje)
	{
		System.out.println(
				"Trimestre 1:"+"\n"
						+ " Numero de zonas cargadas: "+pViajes+"\n"+
						"Zona con SourceId minimo: "+" Sourceid: "+pPrimerViaje.darSourceId()+" Nombre: "+pPrimerViaje.darNombre()+" Perimetro: "+pPrimerViaje.darPerimetro()+" Area: "+pPrimerViaje.darArea()+" Numero Puntos: "+pPrimerViaje.darNumeroCoordenadas()+"\n"+
						"Zona con SourceId maximo: "+" Sourceid: "+pUltimoViaje.darSourceId()+" Nombre: "+pUltimoViaje.darNombre()+" Perimetro: "+pUltimoViaje.darPerimetro()+" Area: "+pUltimoViaje.darArea()+" Numero Puntos: "+pUltimoViaje.darNumeroCoordenadas()+"\n"
				);
	}
	public void printConsultarPorId(Zona z)
	{
		if (z==null)
		{

		}
		else
		{
			System.out.println(" Sourceid: "+z.darSourceId()+" Nombre: "+z.darNombre()+" Perimetro: "+z.darPerimetro()+" Area: "+z.darArea()+" Numero Puntos: "+z.darNumeroCoordenadas());
		}
	}
	public void printConsultarRangoPorId(ArregloDinamico<Zona> ad)
	{
		for (int i=0;i<ad.darTamano();i++)
		{
			if (ad.darElemento(i)==null)
			{
				System.out.println("La zona con llave "+i+"no existe");
			}
			else
			System.out.println(" Sourceid: "+ad.darElemento(i).darSourceId()+" Nombre: "+ad.darElemento(i).darNombre()+" Perimetro: "+ad.darElemento(i).darPerimetro()+" Area: "+ad.darElemento(i).darArea()+" Numero Puntos: "+ad.darElemento(i).darNumeroCoordenadas());
		}
	}
	
	public void printDatosAnalisis(int pTotalNodos, int pAltura, double pAlturaHojas)
	{
		System.out.println("El n�mero total de nodos es: "+pTotalNodos);
		System.out.println("La altura real del �rbol es: "+pAltura);
		System.out.println("La altura promedio de las hojas es: "+pAlturaHojas);
	}






}
