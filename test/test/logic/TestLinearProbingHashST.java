package test.logic;
import model.data_structures.*;

import static org.junit.Assert.*;

import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

public class TestLinearProbingHashST 
{

	private LinearProbingHashST<String, Integer> hashTable;

	@Before
	public void setUp1() 
	{
		hashTable = new LinearProbingHashST<String, Integer>(2);
	}

	public void setUp2() 
	{
		hashTable.put("a", 1);
		hashTable.put("b", 2);
		hashTable.put("c", 3);
	}

	public void setUp3() 
	{
		hashTable.putInSet("a", 1);
		hashTable.putInSet("a", 2);
		hashTable.putInSet("a", 3);
		hashTable.putInSet("b", 1);
		hashTable.putInSet("b", 2);
		hashTable.putInSet("b", 3);
		hashTable.putInSet("c", 1);
		hashTable.putInSet("c", 2);
		hashTable.putInSet("c", 3);
	}

	@Test
	public void testGet2() {
		setUp2();
		assertTrue(hashTable!=null);
		assertEquals(hashTable.darN(),3);
		assertEquals((Integer)1,hashTable.get("a"));
		assertEquals((Integer)2,hashTable.get("b"));
		assertEquals((Integer)3,hashTable.get("c"));
	}
	@Test
	public void testGet3() {
		setUp3();
		assertTrue(hashTable!=null);
		assertEquals(hashTable.darN(),3);

		Iterator<Integer> vals = hashTable.getSet("a");
		int i = 1;
		while (vals.hasNext()) {
			Integer integer = (Integer) vals.next();
			assertEquals((Integer)i,integer);
			i++;
		}

		vals = hashTable.getSet("b");
		i = 1;
		while (vals.hasNext()) {
			Integer integer = (Integer) vals.next();
			assertEquals((Integer)i,integer);
			i++;
		}

		vals = hashTable.getSet("c");
		i = 1;
		while (vals.hasNext()) {
			Integer integer = (Integer) vals.next();
			assertEquals((Integer)i,integer);
			i++;
		}
	}

	@Test
	public void testDelete2() {
		setUp2();
		assertEquals(hashTable.darN(),3);
		assertEquals((Integer)1,hashTable.delete("a"));
		assertEquals(hashTable.darN(),2);
		assertEquals((Integer)2,hashTable.delete("b"));
		assertEquals(hashTable.darN(),1);
		assertEquals((Integer)3,hashTable.delete("c"));
		assertEquals(hashTable.darN(),0);
	}

	@Test
	public void testDelete3() {
		setUp3();
		assertEquals(hashTable.darN(),3);
		Iterator<Integer> vals = hashTable.deleteSet("a");
		int i = 1;
		while (vals.hasNext()) {
			Integer integer = (Integer) vals.next();
			assertEquals((Integer)i,integer);
			i++;
		}
		assertEquals(hashTable.darN(),2);

		vals = hashTable.deleteSet("b");
		i = 1;
		while (vals.hasNext()) {
			Integer integer = (Integer) vals.next();
			assertEquals((Integer)i,integer);
			i++;
		}
		assertEquals(hashTable.darN(),1);

		vals = hashTable.deleteSet("c");
		i = 1;
		while (vals.hasNext()) {
			Integer integer = (Integer) vals.next();
			assertEquals((Integer)i,integer);
			i++;
		}
		assertEquals(hashTable.darN(),0);
	}


}
